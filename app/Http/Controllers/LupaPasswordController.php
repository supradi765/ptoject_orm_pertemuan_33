<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pengguna;

use Illuminate\Support\Str;
use App\Models\PasswordResetTokens;
use App\Mail\LupaPasswordMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class LupaPasswordController extends Controller
{
    public function formLupapassword()
    {
        return view('pengguna.form_lupa_password');
    }

    public function prosesLupaPassword(Request $req)
    {
        $this->validate($req, [
            'email' => 'required|string|email'
        ]);

        try {
            $datas = $req->all();
            // dd($datas);
            $cekData = Pengguna::where('email', $datas['email'])->first();
            if (empty($cekData)) {
                return redirect()->route('forgot.form-forgot')->with('error', __('Email Salah'));
            }

            $token = Str::random(64);

            //proses cek ke tabel PasswordResetTokens
            $dataPasswordResetTokens = PasswordResetTokens::where('email', $datas['email'])->first();
            if (!empty($dataPasswordResetTokens)) {
                PasswordResetTokens::where('email', $datas['email'])->delete();
            }

            $dataSave = new PasswordResetTokens;
            $dataSave->email = $datas['email'];
            $dataSave->token = $token;
            $dataSave->created_at = date('Y-m-d H:i:s');
            $dataSave->save();

            // proses kirim email
            $dataMail = [
                'nama_pengguna' => $cekData->email,
                'url_reset' => env('APP_URL') . '/forgot/reset-password/' . $token,
            ];

            Mail::to($datas['email'])->send(new LupaPasswordMail($dataMail));
            return redirect()->route('forgot.form-forgot')->with('succes', __('silahkan periksa email anda'));
        } catch (\Throwable $th) {
            return redirect()->route('forgot.form-forgot')->with('error', __($th->getMessage()));
        }
    }

    public function passwordReset($token)
    {
        $dataReset = passwordResetTokens::where('token', $token)->whereDate('created_at', '=', date('Y-m-d'))->first();
        if (empty($dataReset)) {
            return redirect()->route('forgot.form-forgot')->with('error', __('Token tidak valid'));
        }
        $token = $token;
        $email = $dataReset->email;

        return view('pengguna.form_atur_ulang_password', compact('email', 'token'));
    }

    public function prosesResetPassword(Request $req)
    {
        $this->validate($req, [
            'password' => 'required|string|min:8|confirmed',
        ]);

        $datas = $req->all();

        Pengguna::where('email', $datas['email'])->update([
            'password' => Hash::make($datas['password'])
        ]);

        passwordResetTokens::where('email', $datas['email'])->delete();

        return redirect()->route('login.form-login')->with('succes', __('Reset password selesai'));
    }
}
